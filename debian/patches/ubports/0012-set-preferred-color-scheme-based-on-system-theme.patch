From 8b6715400b353820e60d5f2c5ecdc557b23281fb Mon Sep 17 00:00:00 2001
From: Maciej Sopylo <me@klh.io>
Date: Wed, 18 Jan 2023 20:34:03 +0100
Subject: [PATCH] set preferred color scheme based on system theme

Signed-off-by: Maciej Sopylo <me@klh.io>
---
 src/core/web_engine_settings.cpp | 45 +++++++++++++++++++++++++-------
 src/core/web_engine_settings.h   |  2 ++
 2 files changed, 37 insertions(+), 10 deletions(-)

diff --git a/src/core/web_engine_settings.cpp b/src/core/web_engine_settings.cpp
index 4115d10d7..988ca526b 100644
--- a/src/core/web_engine_settings.cpp
+++ b/src/core/web_engine_settings.cpp
@@ -57,7 +57,10 @@
 #include "ui/events/event_switches.h"
 #include "ui/native_theme/native_theme.h"
 
+#include <QDebug>
 #include <QFont>
+#include <QSettings>
+#include <QStandardPaths>
 #include <QTimer>
 #include <QTouchDevice>
 
@@ -68,6 +71,7 @@ QHash<WebEngineSettings::FontFamily, QString> WebEngineSettings::s_defaultFontFa
 QHash<WebEngineSettings::FontSize, int> WebEngineSettings::s_defaultFontSizes;
 
 static const int batchTimerTimeout = 0;
+static const QString lomiriSettingsFile = QStringLiteral("%1/lomiri-ui-toolkit/theme.ini");
 
 static inline bool isTouchEventsAPIEnabled() {
     static bool initialized = false;
@@ -105,6 +109,17 @@ WebEngineSettings::WebEngineSettings(WebEngineSettings *_parentSettings)
     QObject::connect(&m_batchTimer, &QTimer::timeout, [this]() {
         doApply();
     });
+
+    // watch the lomiri settings file for changes
+    m_fileSystemWatcher.addPath(lomiriSettingsFile.arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)));
+    QObject::connect(&m_fileSystemWatcher, &QFileSystemWatcher::fileChanged, [this](const QString &path) {
+        if (!m_fileSystemWatcher.files().contains(path)) {
+            qWarning() << "System theme settings file removed!";
+            return;
+        }
+        // re-apply WebEngineSettings when the theme in the config changes to update preferred_color_scheme
+        // doApply();
+    });
 }
 
 WebEngineSettings::~WebEngineSettings()
@@ -405,18 +420,28 @@ void WebEngineSettings::applySettingsToWebPreferences(blink::web_pref::WebPrefer
     prefs->minimum_logical_font_size = fontSize(MinimumLogicalFontSize);
     prefs->default_encoding = defaultTextEncoding().toStdString();
 
+    // set preferred_color_scheme based on the selected LUITK theme
+    QSettings lomiriConfig(lomiriSettingsFile.arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)), QSettings::IniFormat);
+    auto themeName = lomiriConfig.value(QStringLiteral("theme")).toString();
+    if (themeName.endsWith("Dark")) {
+        prefs->preferred_color_scheme = blink::PreferredColorScheme::kDark;
+    } else {
+        prefs->preferred_color_scheme = blink::PreferredColorScheme::kLight;
+    }
+
     // Set the theme colors. Based on chrome_content_browser_client.cc:
     const ui::NativeTheme *webTheme = ui::NativeTheme::GetInstanceForWeb();
-    if (webTheme) {
-        switch (webTheme->GetPreferredColorScheme()) {
-          case ui::NativeTheme::PreferredColorScheme::kDark:
-            prefs->preferred_color_scheme = blink::PreferredColorScheme::kDark;
-            break;
-          case ui::NativeTheme::PreferredColorScheme::kLight:
-            prefs->preferred_color_scheme = blink::PreferredColorScheme::kLight;
-            break;
-        }
-    }
+    // TODO: this might come back once we move to a chromium version with native Qt UI support
+    // if (webTheme) {
+    //     switch (webTheme->GetPreferredColorScheme()) {
+    //       case ui::NativeTheme::PreferredColorScheme::kDark:
+    //         prefs->preferred_color_scheme = blink::PreferredColorScheme::kDark;
+    //         break;
+    //       case ui::NativeTheme::PreferredColorScheme::kLight:
+    //         prefs->preferred_color_scheme = blink::PreferredColorScheme::kLight;
+    //         break;
+    //     }
+    // }
 
     // Apply native CaptionStyle parameters.
     base::Optional<ui::CaptionStyle> style;
diff --git a/src/core/web_engine_settings.h b/src/core/web_engine_settings.h
index d97ff5767..1bd2d8916 100644
--- a/src/core/web_engine_settings.h
+++ b/src/core/web_engine_settings.h
@@ -58,6 +58,7 @@
 #include <QUrl>
 #include <QSet>
 #include <QTimer>
+#include <QFileSystemWatcher>
 
 namespace content {
 class WebContents;
@@ -194,6 +195,7 @@ private:
     static QHash<FontFamily, QString> s_defaultFontFamilies;
     static QHash<FontSize, int> s_defaultFontSizes;
     UnknownUrlSchemePolicy m_unknownUrlSchemePolicy;
+    QFileSystemWatcher m_fileSystemWatcher;
 
     friend class WebContentsAdapter;
 };
-- 
2.39.0

