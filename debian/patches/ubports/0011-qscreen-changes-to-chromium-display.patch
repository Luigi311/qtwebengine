Description: Connect QScreen changes to Chromium
 Make sure Chromium has up-to-date Display info by connecting to each
 QScreen's change signals. Make sure to keep the connection handles
 since Chromium's display, not being a QObject, doesn't benefit from
 automatic disconnect.
 .
 For us, this allows our out-of-tree libhybris-based camera support to
 have up-to-date orientation info to report to Chromium.
Author: Ratchanan Srirattanamet <ratchanan@ubports.com>
Origin: vendor
Bug-UBports: https://gitlab.com/ubports/development/core/morph-browser/-/issues/529
Forwarded: no
Last-Update: 2023-03-17
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/core/desktop_screen_qt.cpp
+++ b/src/core/desktop_screen_qt.cpp
@@ -89,6 +89,13 @@
 {
     for (auto conn : qAsConst(m_connections))
         QObject::disconnect(conn);
+
+    // For each (known) screen (note: iterator gives values)
+    for (auto const & conns : qAsConst(m_screenConnections)) {
+        // For each connection to the screen
+        for (auto conn : conns)
+            QObject::disconnect(conn);
+    }
 }
 
 void DesktopScreenQt::initializeScreens()
@@ -104,7 +111,10 @@
                 updateAllScreens();
             });
         m_connections[2] =
-            QObject::connect(qApp, &QGuiApplication::screenRemoved, [this] (QScreen *) {
+            QObject::connect(qApp, &QGuiApplication::screenRemoved, [this] (QScreen * screen) {
+                // Connections itself should be dropped as soon as the screen is destroyed.
+                m_screenConnections.remove(screen);
+
                 updateAllScreens();
             });
     } else {
@@ -126,8 +136,25 @@
     const int oldLen = GetNumDisplays();
     for (int i = screens.length(); i < oldLen; ++i)
         display_list().RemoveDisplay(i);
-    for (int i = 0; i < screens.length(); ++i)
-        ProcessDisplayChanged(toDisplayDisplay(i, screens.at(i)), i == 0 /* is_primary */);
+    for (int i = 0; i < screens.length(); ++i) {
+        QScreen *screen = screens.at(i);
+        ProcessDisplayChanged(toDisplayDisplay(i, screen), i == 0 /* is_primary */);
+
+        if (!m_screenConnections.contains(screen)) {
+            auto updateFunc = [this, screen] () {
+                int i = qApp->screens().indexOf(screen);
+                ProcessDisplayChanged(toDisplayDisplay(i, screen), i == 0 /* is_primary */);
+            };
+
+            m_screenConnections[screen] = {
+                QObject::connect(screen, &QScreen::availableGeometryChanged, updateFunc),
+                QObject::connect(screen, &QScreen::physicalDotsPerInchChanged, updateFunc),
+                QObject::connect(screen, &QScreen::refreshRateChanged, updateFunc),
+                QObject::connect(screen, &QScreen::orientationChanged, updateFunc),
+                QObject::connect(screen, &QScreen::primaryOrientationChanged, updateFunc),
+            };
+        }
+    }
 
     return screens.length() > 0;
 }
--- a/src/core/desktop_screen_qt.h
+++ b/src/core/desktop_screen_qt.h
@@ -42,7 +42,10 @@
 
 #include "ui/display/screen_base.h"
 
+#include <qscreen.h>
+#include <qmap.h>
 #include <qmetaobject.h>
+#include <qvector.h>
 
 namespace QtWebEngineCore {
 
@@ -58,6 +61,7 @@
     void initializeScreens();
     bool updateAllScreens();
     QMetaObject::Connection m_connections[3];
+    QMap<QScreen *, QVector<QMetaObject::Connection>> m_screenConnections;
 };
 
 } // namespace QtWebEngineCore
